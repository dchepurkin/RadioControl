#include "NRF24.h"
#include <string.h>
#include <cstdio>

NRF24::NRF24(spi_inst_t* spi, uint16_t csn, uint16_t ce)
{
	this->spi = spi;
	this->csn = csn;
	this->ce = ce;

	// This init should be defined externally by the user.
	spi_init(this->spi, 10000000);
	gpio_set_function(2, GPIO_FUNC_SPI);
	gpio_set_function(3, GPIO_FUNC_SPI);
	gpio_set_function(4, GPIO_FUNC_SPI);
	// spi end.

	gpio_init(csn);
	gpio_init(ce);

	gpio_set_dir(csn, 1);
	gpio_set_dir(ce, 1);

	ceLow();
	csnHigh();
}

NRF24::~NRF24()
{
}

uint8_t NRF24::ReadRegister(uint8_t reg)
{
	reg = 0b00011111 & reg;
	uint8_t result = 0;
	csnLow();
	spi_write_blocking(spi, &reg, 1);
	spi_read_blocking(spi, 0xff, &result, 1);
	csnHigh();

	return result;
}

void NRF24::writeCommand(uint8_t cmd)
{
	csnLow();
	spi_write_blocking(spi, &cmd, 1);
	csnHigh();
}

void NRF24::writeReg(uint8_t reg, uint8_t data)
{
	writeReg(reg, &data, 1);
}

void NRF24::writeReg(uint8_t reg, uint8_t* data, uint8_t size)
{
	//     Write bit|  Mask bits
	reg = 0b00100000 | (0b00011111 & reg);
	csnLow();
	spi_write_blocking(spi, &reg, 1);
	spi_write_blocking(spi, data, size);
	csnHigh();
}

void NRF24::enableAck(uint8_t ack)
{
	writeReg(1, ack);
}

void NRF24::config(uint8_t* address, uint8_t InChannel, uint8_t InMessageLen)
{
	//channel = 2;
	channel = InChannel;
	messageLen = InMessageLen;
	csnHigh();
	ceLow();
	sleep_ms(11);

	writeReg(0, 0b00001110); // config CRC, 2Bytes CRD, Powerup.
	sleep_us(1500);

	writeReg(1, 0b0); // disable ack.

	//writeReg(3, 0b00000011); // 5 byte RX/TX address.

	//writeReg(4, 0b00000000); // 250us AutoRetrans delay.

	//writeReg(5, channel); // channel.

	writeReg(6, 0b00001110); // 2Mbs, 0dBm... max power.

	writeReg(0x0a, (uint8_t*)"ChipTX", 6); //RX Adress
	writeReg(0x10, (uint8_t*)"ChipRX", 6); //TX Adress

	writeReg(0x11, InMessageLen); // message Length.
}

void NRF24::modeTX()
{
	if(State == NRFState::TX) { return; }

	FlushTX();

	uint8_t config = ReadRegister(0);
	config &= ~(1 << 0); // clear PRIM_RX bit.
	writeReg(0, config);
	ceLow();

	State = NRFState::TX;
	sleep_us(130); // 130us settling time.
}

void NRF24::modeRX()
{
	if(State == NRFState::RX) { return; }

	FlushRX();

	uint8_t config = ReadRegister(0);
	config |= (1 << 0); // set PRIM_RX bit.
	writeReg(0, config);
	ceHigh();

	State = NRFState::RX;
	sleep_us(130); // 130us settling time.
}

uint8_t NRF24::IsAvailable()
{
	return ((ReadRegister(7) & 0b00001110) < 11);
}

void NRF24::SendMessage(uint8_t* InMessage)
{
	uint8_t flush_tx = 0b11100001;
	uint8_t tx_payload = 0b10100000;

	uint8_t buffer[32] = {0};
	memcpy(buffer, InMessage, messageLen);

	csnLow();
	spi_write_blocking(spi, &tx_payload, 1);
	spi_write_blocking(spi, buffer, messageLen);
	csnHigh();
	ceHigh();

	while((ReadRegister(7) & 0b00110000) == 0) {}

	ceLow();

	writeReg(7, 0b00110000);

	uint8_t observer = ReadRegister(8);
	//if(observer & 0b11110000)
	{
		//	packetsLost += (observer >> 4); // keep track of packet lost.
		writeReg(5, channel); // clear PLOST_CNT by writing channel.
	}
}

void NRF24::GetMessage(uint8_t* buffer)
{
	uint8_t rx_payload = 0b01100001;

	csnLow();
	spi_write_blocking(spi, &rx_payload, 1);
	spi_read_blocking(spi, 0xff, (uint8_t*)buffer, messageLen);
	csnHigh();
}

void NRF24::FlushRX()
{
	csnLow();
	uint8_t Reg = 0xE2;
	spi_write_blocking(spi, &Reg, 1);
	csnHigh();
}

void NRF24::FlushTX()
{
	csnLow();
	uint8_t Reg = 0xE1;
	spi_write_blocking(spi, &Reg, 1);
	csnHigh();
}

