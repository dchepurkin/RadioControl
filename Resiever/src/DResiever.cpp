#include "DResiever.h"
#include "DRadioTypes.h"
#include "hardware/flash.h"

void DResiever::Begin()
{
	//SaveSettingsToFlash();
	ReadSettingsFromFlash();

	SPI RadioSPI;
	RadioSPI.begin(spi0, 2, 3, 4);
	Radio.begin(RadioSPI);
	Radio.setDataRate(RF24_250KBPS);
	Radio.enableAckPayload();
	Radio.openReadingPipe(1, reinterpret_cast<const uint8_t*>("Radio"));
	Radio.startListening();

	uint8_t Message[5];

	uint32_t SaveTime = 1000;
	uint32_t PreTime = to_ms_since_boot(get_absolute_time());

	while(true)
	{
		if(Radio.available())
		{
			PreTime = to_ms_since_boot(get_absolute_time());

			Radio.read(Message, 5);

			const auto Request = static_cast<ERequest>(Message[0]);

			if(Request == ERequest::ChannelsValues)
			{
				const auto Values = *reinterpret_cast<ChannelsPercents*>(Message + 1);
				SetChannelsValues(Values);
			}
			else if(Request == ERequest::ConnectionStatus)
			{
#ifdef DEBUG
				printf("Connection Status Request\n");
#endif

				SendRequest(ERequest::ConnectionStatus);
				SendRequest(ERequest::BatteryStatus);
			}
			else if(Request == ERequest::BatteryStatus)
			{
				const auto Values = *reinterpret_cast<ChannelsPercents*>(Message + 1);
				SetChannelsValues(Values);
				SendRequest(ERequest::BatteryStatus);
			}
			else if(Request == ERequest::RXSettings)
			{
				const auto Settings = *reinterpret_cast<RXSettings*>(Message + 1);
				SetChannelsSettings(Settings);
			}
			else if(Request == ERequest::GetRXSettings)
			{
				SendRequest(ERequest::RXSettings);
			}
			else if(Request == ERequest::SaveRXSettings)
			{
				SaveSettingsToFlash();
				SendRequest(ERequest::BatteryStatus);
			}
		}
		else
		{
			//������ ��� ������ �����
			const auto CurrentTime = to_ms_since_boot(get_absolute_time());
			if(CurrentTime - PreTime >= SaveTime)
			{
				ChannelsPercents SaveValues{50, 50, 50, 50};
				SetChannelsValues(SaveValues);

				PreTime = CurrentTime;
			}
		}
	}
}

uint8_t DResiever::GetBatteryStatus()
{
	return Ina.GetBatteryLevel();
}

void DResiever::SendRequest(ERequest InRequest)
{
	uint8_t Message[5];

	if(InRequest == ERequest::ConnectionStatus)
	{
		Message[0] = ConnectAnswer;
	}
	else if(InRequest == ERequest::BatteryStatus)
	{
		Message[0] = static_cast<uint8_t>(ERequest::BatteryStatus);
		Message[1] = GetBatteryStatus();
	}
	else if(InRequest == ERequest::RXSettings)
	{
		RXSettings Settings{Engine.GetMaxPower(), Channel2Servo.GetOffset(), Channel3Servo.GetOffset(), Channel4Servo.GetOffset()};
		const auto SettingsPtr = reinterpret_cast<uint8_t*>(&Settings);

		Message[0] = static_cast<uint8_t>(InRequest);

		for(uint8_t i = 0; i < sizeof(RXSettings); ++i)
		{
			Message[i + 1] = SettingsPtr[i];
		}
	}

	Radio.writeAckPayload(1, Message, 5);
}

void DResiever::ReadSettingsFromFlash()
{
	RXSettings Settings = *reinterpret_cast<RXSettings*>(XIP_BASE + SETTINGS_OFFSET);
	SetChannelsSettings(Settings);
}

void DResiever::SaveSettingsToFlash()
{
#ifdef DEBUG
	printf("Save settings\n");
#endif

	RXSettings Settings{Engine.GetMaxPower(), Channel2Servo.GetOffset(), Channel3Servo.GetOffset(), Channel4Servo.GetOffset()};
	const auto SettingsByte = reinterpret_cast<uint8_t*>(&Settings);

	uint8_t Buffer[FLASH_PAGE_SIZE];

	for(uint8_t i = 0; i < sizeof(RXSettings); ++i)
	{
		Buffer[i] = SettingsByte[i];
	}

	flash_range_erase(SETTINGS_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(SETTINGS_OFFSET, Buffer, FLASH_PAGE_SIZE);

	sleep_ms(100);
}

void DResiever::SetChannelsSettings(const RXSettings& InSettings)
{
	Engine.SetMaxPower(InSettings.MaxEnginePower);
	Channel2Servo.SetOffset(InSettings.Channel2ServoOffset);
	Channel3Servo.SetOffset(InSettings.Channel3ServoOffset);
	Channel4Servo.SetOffset(InSettings.Channel4ServoOffset);
}

void DResiever::SetChannelsValues(const ChannelsPercents& InValues)
{
#ifdef DEBUG
	printf("1 = %i, 2 = %i, 3 = %i, 4 = %i\n", InValues.Channel1, InValues.Channel2, InValues.Channel3, InValues.Channel4);
#endif

	Engine.SetPosition(InValues.Channel1);
	Channel2Servo.SetPosition(InValues.Channel2);
	Channel3Servo.SetPosition(InValues.Channel3);
	Channel4Servo.SetPosition(InValues.Channel4);
}
