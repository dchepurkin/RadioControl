#include "DLora.h"

DLora::DLora()
{
	if(Lora.begin(433E6))
	{
#ifdef DEBUG
		printf("Lora init success\n");
#endif
	}
#ifdef DEBUG
	else
	{
		printf("Lora init failed\n");
	}
#endif
}

void DLora::Send(const uint8_t* InMessage, const uint8_t InSize)
{
	Lora.beginPacket();
	Lora.write(InMessage, InSize);
	Lora.endPacket();
}

int DLora::Read()
{
	return Lora.read();
}

int DLora::ParsePacket()
{
	return Lora.parsePacket();
}
