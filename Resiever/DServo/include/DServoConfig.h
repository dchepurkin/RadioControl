#pragma once

#include "pico/stdlib.h"

#define SERVO_0DEG 409
#define SERVO_45DEG 819
#define SERVO_90DEG 1229
#define SERVO_135DEG 1638
#define SERVO_180DEG 2048

#define SERVO_MIN 819
#define SERVO_MAX 1638

#define ENGINE_MIN 1100
#define ENGINE_MAX 1650

#define PWM_MIN 0
#define PWM_MAX 4095

#define SERVO_1_DEGREE 9.1f
