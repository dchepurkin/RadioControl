#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "DServoConfig.h"

class DEngine
{
public:
	explicit DEngine(const uint InPin);

	void SetPosition(const uint InValue) const;

	void SetMaxPower(const uint8_t InPowerPercent) { MaxPower = InPowerPercent; }

	uint8_t GetMaxPower() const { return MaxPower; }

private:
	uint8_t MaxPower = 100; //[10, 100]

	uint Pin;

	uint Slice;

	uint Channel;

	uint16_t GetMappedValue(uint8_t InPercentValue) const;
};