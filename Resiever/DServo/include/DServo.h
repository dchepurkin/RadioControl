#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "DServoConfig.h"

class DServo
{
public:
	explicit DServo(const uint InPin);

	void SetPosition(const uint InValue) const;

	void SetOffset(const int8_t InOffset) { Offset = InOffset; }

	int8_t GetOffset() const { return Offset; }

private:
	int8_t Offset = 0; //[-10, 10]

	uint Pin;

	uint Slice;

	uint Channel;

	uint16_t GetMappedValue(const uint8_t InPercentValue, const uint16_t OutMin, const uint16_t OutMax) const;
};