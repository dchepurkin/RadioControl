#include "DServo.h"
#include "hardware/pwm.h"
#include <cmath>

DServo::DServo(const uint InPin)
		: Pin(InPin)
{
	gpio_set_function(Pin, GPIO_FUNC_PWM);
	Slice = pwm_gpio_to_slice_num(Pin);
	Channel = pwm_gpio_to_channel(Pin);

	pwm_set_clkdiv(Slice, 152.4f);
	pwm_set_wrap(Slice, 16383);
	pwm_set_enabled(Slice, true);

	//pwm_set_chan_level(Slice, Channel, SERVO_90DEG);
}

void DServo::SetPosition(const uint InValue) const
{
	const auto ServoValue = GetMappedValue(InValue, SERVO_MIN, SERVO_MAX);
	pwm_set_chan_level(Slice, Channel, ServoValue);
}

uint16_t DServo::GetMappedValue(const uint8_t InPercentValue, const uint16_t OutMin, const uint16_t OutMax) const
{
	const float Calibration = static_cast<float>(Offset) * SERVO_1_DEGREE * 2;

	return static_cast<uint16_t>(ceil((static_cast<double>(OutMax - OutMin) / 100) * InPercentValue + static_cast<double>(OutMin)) + Calibration);
}
