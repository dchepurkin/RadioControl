#include "DEngine.h"
#include "hardware/pwm.h"
#include <cmath>

DEngine::DEngine(const uint InPin)
		: Pin(InPin)
{
	gpio_set_function(Pin, GPIO_FUNC_PWM);
	Slice = pwm_gpio_to_slice_num(Pin);
	Channel = pwm_gpio_to_channel(Pin);

	pwm_set_clkdiv(Slice, 152.4f);
	pwm_set_wrap(Slice, 16383);
	pwm_set_enabled(Slice, true);

	//pwm_set_chan_level(Slice, Channel, SERVO_90DEG);
}

void DEngine::SetPosition(const uint InValue) const
{
	const auto ServoValue = GetMappedValue(InValue);
	pwm_set_chan_level(Slice, Channel, ServoValue);
}

uint16_t DEngine::GetMappedValue(uint8_t InPercentValue) const
{
	if(InPercentValue > 55)
	{
		InPercentValue = InPercentValue * 2 - 100;
		if(InPercentValue > MaxPower) { InPercentValue = MaxPower; }
		return static_cast<uint16_t>(ceil((static_cast<double>(ENGINE_MAX - SERVO_90DEG) / 100) * InPercentValue + static_cast<double>(SERVO_90DEG)));
	}
	else if(InPercentValue < 45)
	{
		InPercentValue = (50 - InPercentValue) * 2;
		return static_cast<uint16_t>(ceil((static_cast<double>(ENGINE_MIN - SERVO_90DEG) / 100) * InPercentValue + static_cast<double>(SERVO_90DEG)));
	}
	else
	{
		return SERVO_90DEG;
	}
}
