#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "DRadioTypes.h"
#include "DServo.h"
#include "DEngine.h"
#include "RF24.h"
#include "DIna219.h"

class DResiever
{
public:
	void Begin();

private:
#ifdef DEBUG
	RF24 Radio{5, 6};
#else
	RF24 Radio{0, 1};
#endif

	DIna219 Ina{8, 9};

	DServo Channel2Servo{14};

	DServo Channel3Servo{15};

	DServo Channel4Servo{26};

	DEngine Engine{27};

	uint8_t GetBatteryStatus();

	void SendRequest(ERequest InRequest);

	void ReadSettingsFromFlash();

	void SaveSettingsToFlash();

	void SetChannelsSettings(const RXSettings& InSettings);

	void SetChannelsValues(const ChannelsPercents& InValues);
};