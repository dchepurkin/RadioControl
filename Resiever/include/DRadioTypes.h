#pragma once

#include "pico/stdlib.h"

const uint8_t ConnectAnswer = 155;

#define SETTINGS_OFFSET (PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE)

enum class ERequest : uint8_t
{
	ConnectionStatus,
	BatteryStatus,
	ChannelsValues,
	GetRXSettings,
	RXSettings,
	SaveRXSettings,
};

struct ChannelsPercents
{
public:
	int8_t Channel1 = 0;

	int8_t Channel2 = 0;

	int8_t Channel3 = 0;

	int8_t Channel4 = 0;
};

struct RXSettings
{
	uint8_t MaxEnginePower = 100;

	int8_t Channel2ServoOffset = 0;

	int8_t Channel3ServoOffset = 0;

	int8_t Channel4ServoOffset = 0;
};