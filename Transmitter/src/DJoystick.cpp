#include "DJoystick.h"
#include "hardware/adc.h"
#include <algorithm>

DJoystick::DJoystick(const uint InHPin, const uint InVPin)
		: HPin(InHPin),
		  VPin(InVPin)
{
	adc_init();
	adc_gpio_init(HPin);
	adc_gpio_init(VPin);
}

int8_t DJoystick::GetHValue()
{
	auto Result = GetValue(HPin, HMinMax);
	return bIsHInverted ? 100 - Result : Result;
}

int8_t DJoystick::GetVValue()
{
	auto Result = GetValue(VPin, VMinMax);
	return bIsVInverted ? 100 - Result : Result;
}

int8_t DJoystick::GetValue(uint InPin, const DMinMaxChannel& InMinMaxInfo)
{
	const auto ADCValue = GetADCValue(InPin);
	return std::clamp((((ADCValue - InMinMaxInfo.Min) * 100) / (InMinMaxInfo.Max - InMinMaxInfo.Min)), 0, 100);
}

void DJoystick::SetMinMax(const DMinMaxChannel& InVMinMaxInfo, const DMinMaxChannel& InHMinMaxInfo)
{
	HMinMax = InHMinMaxInfo;
	VMinMax = InVMinMaxInfo;
}

uint16_t DJoystick::GetHADCValue()
{
	return GetADCValue(HPin);
}

uint16_t DJoystick::GetVADCValue()
{
	return GetADCValue(VPin);
}

uint16_t DJoystick::GetADCValue(uint InPin)
{
	adc_select_input(ADCChannels[InPin]);
	return adc_read();
}

void DJoystick::SetInverted(bool Horizontal, bool Vertical)
{
	bIsHInverted = Horizontal;
	bIsVInverted = Vertical;
}




