#include "DTransmitter.h"
#include "DState/DStateConnecting.h"
#include "hardware/flash.h"

void DTransmitter::Begin()
{
	//NRF Config
	SPI RadioSPI;
	RadioSPI.begin(spi0, 2, 3, 4);
	Radio.begin(RadioSPI);
	Radio.setDataRate(RF24_250KBPS);
	Radio.enableAckPayload();
	//Radio.setRetries(5, 5);
	Radio.openWritingPipe(reinterpret_cast<const uint8_t*>("Radio"));

	//SaveJoysticksInfo(DChannelsInfo{});
	ReadSettingsFromFlash();

	//Screen init
	Screen.SetRotation(ERotation::Degree90);
	//End Screen init

	//Bind Buttons
	EnterButton.SetHoldDelay(1000);
	EnterButton.SetHoldTickDelta(0);

	UpButton.OnPressed.Bind(this, &DTransmitter::OnUpButtonPressed);
	LeftButton.OnPressed.Bind(this, &DTransmitter::OnLeftButtonPressed);
	DownButton.OnPressed.Bind(this, &DTransmitter::OnDownButtonPressed);
	RightButton.OnPressed.Bind(this, &DTransmitter::OnRightButtonPressed);
	EnterButton.OnPressed.Bind(this, &DTransmitter::OnEnterButtonPressed);
	EnterButton.OnHoldDown.Bind(this, &DTransmitter::OnEnterButtonHold);
	//End Bind Buttons

	SetState(&StateConnecting);

	while(true)
	{
		if(State)
		{
			State->Tick();
		}

		UpButton.Tick();
		LeftButton.Tick();
		DownButton.Tick();
		RightButton.Tick();
		EnterButton.Tick();
	}
}

void DTransmitter::SetState(DStateBase* InState)
{
	if(!InState) { return; }

	InState->Begin(this);
	State = InState;
}

uint8_t DTransmitter::GetBatteryStatus()
{
	return Ina.GetBatteryLevel();
}

void DTransmitter::OnUpButtonPressed()
{
	State->UpButtonPressed();
}

void DTransmitter::OnLeftButtonPressed()
{
	State->LeftButtonPressed();
}

void DTransmitter::OnDownButtonPressed()
{
	State->DownButtonPressed();
}

void DTransmitter::OnRightButtonPressed()
{
	State->RightButtonPressed();
}

void DTransmitter::OnEnterButtonPressed()
{
	State->EnterButtonPressed();
}

void DTransmitter::OnEnterButtonHold()
{
	State->EnterButtonHoldDown();
}

void DTransmitter::SendRequest(ERequest InRequest)
{
	uint8_t Message[5];
	Message[0] = static_cast<uint8_t>(InRequest);

	if(InRequest == ERequest::ChannelsValues || InRequest == ERequest::BatteryStatus)
	{
		auto ChannelsPercents = GetChannelsPercents();

		const auto ValuesBytes = reinterpret_cast<uint8_t*>(&ChannelsPercents);

		for(uint8_t i = 0; i < sizeof(ChannelsPercents); ++i)
		{
			Message[i + 1] = ValuesBytes[i];
		}
	}
	else if(InRequest == ERequest::RXSettings)
	{
		const auto ValuesBytes = reinterpret_cast<uint8_t*>(&RxSettings);

		for(uint8_t i = 0; i < sizeof(RXSettings); ++i)
		{
			Message[i + 1] = ValuesBytes[i];
		}
	}

	Radio.write(Message, 5);
}

void DTransmitter::SetRXSettings(const RXSettings& InSettings)
{
	RxSettings = InSettings;
}

void DTransmitter::AddRXSettingsValues(const RXSettings& InSettings)
{
	RxSettings.MaxEnginePower = std::clamp(RxSettings.MaxEnginePower + InSettings.MaxEnginePower, 10, 100);
	RxSettings.Channel2ServoOffset = std::clamp(RxSettings.Channel2ServoOffset + InSettings.Channel2ServoOffset, -10, 10);
	RxSettings.Channel3ServoOffset = std::clamp(RxSettings.Channel3ServoOffset + InSettings.Channel3ServoOffset, -10, 10);
	RxSettings.Channel4ServoOffset = std::clamp(RxSettings.Channel4ServoOffset + InSettings.Channel4ServoOffset, -10, 10);
}

void DTransmitter::SaveCalibrationInfo(const DChannelsInfo& InInfo)
{
#ifdef DEBUG
	printf("Save Calibration settings\n");
#endif

	const auto SettingsByte = reinterpret_cast<const uint8_t*>(&InInfo);

	uint8_t Buffer[FLASH_PAGE_SIZE];

	for(uint8_t i = 0; i < sizeof(DChannelsInfo); ++i)
	{
		Buffer[i] = SettingsByte[i];
	}

	flash_range_erase(CALIBRATION_SETTINGS_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(CALIBRATION_SETTINGS_OFFSET, Buffer, FLASH_PAGE_SIZE);
	UpdateJoysticksCalibration(InInfo);
}

void DTransmitter::SaveInvertInfo(const DChannelsInvertInfo& InInfo)
{
#ifdef DEBUG
	printf("Save Invert settings\n");
#endif

	const auto SettingsByte = reinterpret_cast<const uint8_t*>(&InInfo);

	uint8_t Buffer[FLASH_PAGE_SIZE];

	for(uint8_t i = 0; i < sizeof(DChannelsInvertInfo); ++i)
	{
		Buffer[i] = SettingsByte[i];
	}

	flash_range_erase(INVERT_SETTINGS_OFFSET, FLASH_SECTOR_SIZE);
	flash_range_program(INVERT_SETTINGS_OFFSET, Buffer, FLASH_PAGE_SIZE);
	UpdateJoysticksInvertion(InInfo);
}

void DTransmitter::ReadSettingsFromFlash()
{
	DChannelsInfo CalibrationSettings = *reinterpret_cast<DChannelsInfo*>(XIP_BASE + CALIBRATION_SETTINGS_OFFSET);
	DChannelsInvertInfo InvertSettings = *reinterpret_cast<DChannelsInvertInfo*>(XIP_BASE + INVERT_SETTINGS_OFFSET);

	UpdateJoysticksCalibration(CalibrationSettings);
	UpdateJoysticksInvertion(InvertSettings);
}

void DTransmitter::UpdateJoysticksCalibration(const DChannelsInfo& InSettings)
{
	LeftJoystick.SetMinMax(InSettings.LeftV, InSettings.LeftH);
	RightJoystick.SetMinMax(InSettings.RightV, InSettings.RightH);
}

void DTransmitter::UpdateJoysticksInvertion(const DChannelsInvertInfo& InSettings)
{
	LeftJoystick.SetInverted(InSettings.bIsLeftHInverted, InSettings.bIsLeftVInverted);
	RightJoystick.SetInverted(InSettings.bIsRightHInverted, InSettings.bIsRightVInverted);
}

void DTransmitter::GetInvertInfo(DChannelsInvertInfo& OutInfo)
{
	OutInfo.bIsLeftVInverted = LeftJoystick.IsVInverted();
	OutInfo.bIsLeftHInverted = LeftJoystick.IsHInverted();

	OutInfo.bIsRightVInverted = RightJoystick.IsVInverted();
	OutInfo.bIsRightHInverted = RightJoystick.IsHInverted();
}

ChannelsPercents DTransmitter::GetChannelsPercents()
{
	return {LeftJoystick.GetVValue()
			, LeftJoystick.GetHValue()
			, RightJoystick.GetVValue()
			, RightJoystick.GetHValue()
	};
}

ChannelsValues DTransmitter::GetChannelsValues()
{
	return {LeftJoystick.GetVADCValue()
			, LeftJoystick.GetHADCValue()
			, RightJoystick.GetVADCValue()
			, RightJoystick.GetHADCValue()
	};
}







