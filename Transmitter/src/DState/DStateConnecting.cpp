#ifdef DEBUG
	#include <cstdio>
#endif

#include "DState/DStateConnecting.h"
#include "DState/DStateIdle.h"
#include "DState/DStateTXSettings.h"
#include "DRadioTypes.h"
#include "DColors.h"
#include "UI/DStartScreen.h"
#include "UI/DConnectionText.h"
#include "UI/DConnectionSuccess.h"
#include "DTransmitter.h"
#include <cmath>

void DStateConnecting::OnBegin()
{
#ifdef DEBUG
	printf("State Connecting enabled\n");
#endif
	IsConnected = false;
}

void DStateConnecting::DrawUI()
{
	Screen->FillScreen(BLACK);
	Screen->DrawBitmap({40, 40}, {240, 90}, StartScreen);
}

void DStateConnecting::Tick()
{
	DStateBase::Tick();

	if(!IsConnected)
	{
		auto CurrentScreenTime = to_ms_since_boot(get_absolute_time());
		if(CurrentScreenTime - PreScreenTime >= 120)
		{
			PreScreenTime = CurrentScreenTime;
			DrawConnectionScreen();
		}

		if(DeltaTime >= 100)
		{
#ifdef DEBUG
			printf("Connecting\n");
#endif

			Transmitter->SendRequest(ERequest::ConnectionStatus);
			PreTime = CurrentTime;
		}

		if(Radio->isAckPayloadAvailable())
		{
			uint8_t Message[5];
			Radio->read(Message, 5);

			if(Message[0] == ConnectAnswer)
			{
#ifdef DEBUG
				printf("Connecting success\n");
#endif
				IsConnected = true;
				DeltaTime = CurrentTime;
				DrawConnectionSuccess();
			}
		}
	}
	else
	{
		if(DeltaTime == 1500)
		{
			SetState(&StateIdle);
		}
	}
}

void DStateConnecting::DrawConnectionScreen()
{
	if(ScreenIndex == 6)
	{
		ScreenIndex = 0;
		Screen->DrawFillRect({66, 72}, {187, 26}, BLACK);
	}
	else
	{
		const auto Offset = (-abs(ScreenIndex - 3) + 3) * ConnectionText_Offset;
		Screen->DrawBitmap({66, 72}, {187, 26}, ConnectionText + Offset);

		++ScreenIndex;
	}
}

void DStateConnecting::DrawConnectionSuccess()
{
	Screen->DrawBitmap({40, 40}, {240, 90}, ConnectionSuccess);
}

void DStateConnecting::EnterButtonHoldDown()
{
	StateTXSettings.SetConnectingOnQuit();
	SetState(&StateTXSettings);
}
