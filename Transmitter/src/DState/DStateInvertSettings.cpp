#include "DTransmitter.h"
#include "DState/DStateInvertSettings.h"
#include "DState/DStateTXSettings.h"
#include "DColors.h"

DStateInvertSettings::DStateInvertSettings()
{
	SettingsMenu.Add("����� 1 - �����");
	SettingsMenu.Add("����� 2 - �����");
	SettingsMenu.Add("����� 3 - �����");
	SettingsMenu.Add("����� 4 - �����");
	SettingsMenu.Add("��������� � �����");
}

void DStateInvertSettings::OnBegin()
{
	Transmitter->GetInvertInfo(InvertInfo);
	ShowInvertSettings();
}

void DStateInvertSettings::DrawUI()
{
	Screen->DrawWindow("�������������� �������");
	SettingsMenu.Show(Screen);
}

void DStateInvertSettings::UpButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::UP);
}

void DStateInvertSettings::DownButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::DOWN);
}

void DStateInvertSettings::EnterButtonPressed()
{
	const auto CursorPosition = SettingsMenu.GetCursorPosition();
	switch(CursorPosition)
	{
		case 0:
		{
			InvertInfo.bIsLeftVInverted = !InvertInfo.bIsLeftVInverted;
			break;
		}
		case 1:
		{
			InvertInfo.bIsLeftHInverted = !InvertInfo.bIsLeftHInverted;
			break;
		}
		case 2:
		{
			InvertInfo.bIsRightVInverted = !InvertInfo.bIsRightVInverted;
			break;
		}
		case 3:
		{
			InvertInfo.bIsRightHInverted = !InvertInfo.bIsRightHInverted;
			break;
		}
		case 4:
		{
			Transmitter->SaveInvertInfo(InvertInfo);
			SettingsMenu.SetCursorPosition(0);
			SetState(&StateTXSettings);
			return;
		}
	}

	Transmitter->UpdateJoysticksInvertion(InvertInfo);
	ShowInvertSettings();
}

void DStateInvertSettings::Tick()
{
	DStateBase::Tick();

	if(DeltaTime > IdleTXDelay)
	{
		Transmitter->SendRequest(ERequest::ChannelsValues);
		PreTime = CurrentTime;
	}
}

void DStateInvertSettings::ShowInvertSettings()
{
	if(!IsActive()) { return; }

	Screen->DrawFillRect({230, 40}, {50, 91}, BLACK);

	Screen->PrintString(InvertInfo.bIsLeftVInverted ? "���" : "����", {280, 40}, ETextAlign::Right);
	Screen->PrintString(InvertInfo.bIsLeftHInverted ? "���" : "����", {280, 65}, ETextAlign::Right);
	Screen->PrintString(InvertInfo.bIsRightVInverted ? "���" : "����", {280, 90}, ETextAlign::Right);
	Screen->PrintString(InvertInfo.bIsRightHInverted ? "���" : "����", {280, 115}, ETextAlign::Right);
}
