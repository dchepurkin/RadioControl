#include "DState/DStateTXSettings.h"
#include "DState/DState�alibrationTX.h"
#include "DState/DStateSettings.h"
#include "DState/DStateInvertSettings.h"
#include "DState/DStateChannelsValues.h"
#include "DState/DStateConnecting.h"

DStateTXSettings::DStateTXSettings()
{
	SettingsMenu.Add("����������");
	SettingsMenu.Add("�������������� �������");
	SettingsMenu.Add("�������� �������");
	SettingsMenu.Add("�����");
}

void DStateTXSettings::OnBegin()
{
}

void DStateTXSettings::DrawUI()
{
	Screen->DrawWindow("��������� ������");
	SettingsMenu.Show(Screen);
}

void DStateTXSettings::UpButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::UP);
}

void DStateTXSettings::DownButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::DOWN);
}

void DStateTXSettings::EnterButtonPressed()
{
	const auto CursorPosition = SettingsMenu.GetCursorPosition();
	switch(CursorPosition)
	{
		case 0:
		{
			SetState(&StateCalibrationTX);
			break;
		}
		case 1:
		{
			SetState(&StateInvertSettings);
			break;
		}
		case 2:
		{
			SetState(&StateChannelsValues);
			break;
		}
		case 3:
		{
			SettingsMenu.SetCursorPosition(0);
			if(bIsConnectingOnQuit)
			{
				bIsConnectingOnQuit = false;
				SetState(&StateConnecting);
			}
			else
			{
				SetState(&StateSettings);
			}

			break;
		}
		default: break;
	}
}
