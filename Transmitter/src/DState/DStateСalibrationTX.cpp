#include "DState/DState�alibrationTX.h"
#include "DState/DStateSettings.h"
#include "DTransmitter.h"
#include "DColors.h"
#include "DState/DStateConnecting.h"

void DStateCalibrationRX::OnBegin()
{
#ifdef DEBUG
	printf("State �alibrationTX enabled\n");
#endif

	Settings = DChannelsInfo{};
}

void DStateCalibrationRX::DrawUI()
{
	Screen->DrawWindow("���������� ������");
	State = ECalibrationState::LeftVerticalMin;
	ShowHelpText();
}

void DStateCalibrationRX::EnterButtonPressed()
{
	switch(State)
	{
		case ECalibrationState::LeftVerticalMin:
		{
			Settings.LeftV.Min = Transmitter->GetLeftJoystick().GetVADCValue();
			break;
		}
		case ECalibrationState::LeftVerticalMax:
		{
			Settings.LeftV.Max = Transmitter->GetLeftJoystick().GetVADCValue();
			break;
		}
		case ECalibrationState::LeftHorizontalMin:
		{
			Settings.LeftH.Min = Transmitter->GetLeftJoystick().GetHADCValue();
			break;
		}
		case ECalibrationState::LeftHorizontalMax:
		{
			Settings.LeftH.Max = Transmitter->GetLeftJoystick().GetHADCValue();
			break;
		}
		case ECalibrationState::RightVerticalMin:
		{
			Settings.RightV.Min = Transmitter->GetRightJoystick().GetVADCValue();
			break;
		}
		case ECalibrationState::RightVerticalMax:
		{
			Settings.RightV.Max = Transmitter->GetRightJoystick().GetVADCValue();
			break;
		}
		case ECalibrationState::RightHorizontalMin:
		{
			Settings.RightH.Min = Transmitter->GetRightJoystick().GetHADCValue();
			break;
		}
		case ECalibrationState::RightHorizontalMax:
		{
			Settings.RightH.Max = Transmitter->GetRightJoystick().GetHADCValue();
			Transmitter->SaveCalibrationInfo(Settings);
			break;
		}
		default:break;
	}
	State = static_cast<ECalibrationState>(static_cast<uint8_t>(State) + 1);
	ShowHelpText();
}

void DStateCalibrationRX::ShowHelpText()
{
	switch(State)
	{
		case ECalibrationState::LeftVerticalMin:
		{
			Screen->PrintString("���������� �����", {160, 55}, ETextAlign::Center);
			Screen->PrintString("�������� � ������", {160, 85}, ETextAlign::Center);
			Screen->PrintString("��������� � ������� ��", {160, 115}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::LeftVerticalMax:
		{
			Screen->PrintString("�������� � �������", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::LeftHorizontalMin:
		{
			Screen->DrawFillRect({0, 85}, {320, 16}, BLACK);
			Screen->PrintString("�������� � �����", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::LeftHorizontalMax:
		{
			Screen->PrintString("�������� � ������", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::RightVerticalMin:
		{
			Screen->PrintString("���������� ������", {160, 55}, ETextAlign::Center);
			Screen->PrintString("�������� � ������", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::RightVerticalMax:
		{
			Screen->PrintString("�������� � �������", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::RightHorizontalMin:
		{
			Screen->DrawFillRect({0, 85}, {320, 16}, BLACK);
			Screen->PrintString("�������� � �����", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::RightHorizontalMax:
		{
			Screen->PrintString("�������� � ������", {160, 85}, ETextAlign::Center);
			break;
		}
		case ECalibrationState::Done:
		{
			SetState(&StateSettings);
			break;
		}
	}
}
