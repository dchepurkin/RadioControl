#include "DState/DStateBase.h"
#include "DTransmitter.h"

void DStateBase::Begin(DTransmitter* InTransmitter)
{
	Transmitter = InTransmitter;
	Radio = Transmitter->GetRadio();
	Screen = Transmitter->GetScreen();

	bIsActive = true;

	DrawUI();
	OnBegin();
}

void DStateBase::SetState(DStateBase* InState)
{
	if(!Transmitter) { return; }

	bIsActive = false;
	Transmitter->SetState(InState);
}

void DStateBase::Tick()
{
	CurrentTime = to_ms_since_boot(get_absolute_time());
	DeltaTime = CurrentTime - PreTime;
}
