#include "DState/DStateRXSettings.h"
#include "DState/DStateSettings.h"
#include "DTransmitter.h"
#include "DColors.h"

DStateRXSettings::DStateRXSettings()
{
	Menu.Add("����� 1 - �����");
	Menu.Add("����� 2 - �����");
	Menu.Add("����� 3 - �����");
	Menu.Add("����� 4 - �����");
	Menu.Add("��������� � �����");
}

void DStateRXSettings::OnBegin()
{
#ifdef DEBUG
	printf("State RXSettings enabled\n");
#endif

	bIsSettingsUpdated = false;
	Request = ERequest::ChannelsValues;
}

void DStateRXSettings::DrawUI()
{
	Screen->DrawWindow("��������� ���������");
	Menu.Show(Screen);
}

void DStateRXSettings::UpButtonPressed()
{
	Menu.MoveCursor(EDirection::UP);
}

void DStateRXSettings::DownButtonPressed()
{
	Menu.MoveCursor(EDirection::DOWN);
}

void DStateRXSettings::LeftButtonPressed()
{
	if(!bIsSettingsUpdated) { return; }

	RXSettings Settings{0, 0, 0, 0};
	constexpr int8_t EnginePowerStep = -5;
	constexpr int8_t ServoStep = -1;
	const auto CursorPosition = Menu.GetCursorPosition();
	switch(CursorPosition)
	{
		case 0:
		{
			Settings.MaxEnginePower = EnginePowerStep;
			break;
		}
		case 1:
		{
			Settings.Channel2ServoOffset = ServoStep;
			break;
		}
		case 2:
		{
			Settings.Channel3ServoOffset = ServoStep;
			break;
		}
		case 3:
		{
			Settings.Channel4ServoOffset = ServoStep;
			break;
		}
		default:break;
	}
	Transmitter->AddRXSettingsValues(Settings);
	ShowRXSettings();
	Request = ERequest::RXSettings;
}

void DStateRXSettings::RightButtonPressed()
{
	if(!bIsSettingsUpdated) { return; }

	RXSettings Settings{0, 0, 0, 0};
	constexpr int8_t EnginePowerStep = 5;
	constexpr int8_t ServoStep = 1;
	const auto CursorPosition = Menu.GetCursorPosition();
	switch(CursorPosition)
	{
		case 0:
		{
			Settings.MaxEnginePower = EnginePowerStep;
			break;
		}
		case 1:
		{
			Settings.Channel2ServoOffset = ServoStep;
			break;
		}
		case 2:
		{
			Settings.Channel3ServoOffset = ServoStep;
			break;
		}
		case 3:
		{
			Settings.Channel4ServoOffset = ServoStep;
			break;
		}
		default:break;
	}
	Transmitter->AddRXSettingsValues(Settings);
	ShowRXSettings();
	Request = ERequest::RXSettings;
}

void DStateRXSettings::EnterButtonPressed()
{
	const auto CursorPosition = Menu.GetCursorPosition();
	if(CursorPosition == 4)
	{
		Request = ERequest::SaveRXSettings;
		Menu.SetCursorPosition(0);
	}
}

void DStateRXSettings::ShowRXSettings()
{
	if(!IsActive()) { return; }

	const auto& Settings = Transmitter->GetRXSettings();

	Screen->DrawFillRect({244, 40}, {50, 91}, BLACK);

	const auto Channel2Str = Settings.Channel2ServoOffset > 0
					   ? "+" + std::to_string(Settings.Channel2ServoOffset)
					   : std::to_string(Settings.Channel2ServoOffset);

	const auto Channel3Str = Settings.Channel3ServoOffset > 0
							 ? "+" + std::to_string(Settings.Channel3ServoOffset)
							 : std::to_string(Settings.Channel3ServoOffset);

	const auto Channel4Str = Settings.Channel4ServoOffset > 0
							 ? "+" + std::to_string(Settings.Channel4ServoOffset)
							 : std::to_string(Settings.Channel4ServoOffset);

	Screen->PrintString(std::to_string(Settings.MaxEnginePower).c_str(), {280, 40}, ETextAlign::Right);
	Screen->PrintString(Channel2Str.c_str(), {280, 65}, ETextAlign::Right);
	Screen->PrintString(Channel3Str.c_str(), {280, 90}, ETextAlign::Right);
	Screen->PrintString(Channel4Str.c_str(), {280, 115}, ETextAlign::Right);
}

void DStateRXSettings::Tick()
{
	DStateBase::Tick();

	if(!bIsSettingsUpdated)
	{
		if(DeltaTime >= 100)
		{
			Transmitter->SendRequest(ERequest::GetRXSettings);
			PreTime = CurrentTime;
		}

		if(Radio->isAckPayloadAvailable())
		{
			uint8_t Message[5];
			Radio->read(Message, 5);

			const auto RXRequest = static_cast<ERequest>(Message[0]);
			if(RXRequest == ERequest::RXSettings)
			{
				const auto Settings = *reinterpret_cast<RXSettings*>(Message + 1);
				Transmitter->SetRXSettings(Settings);
				ShowRXSettings();

				PreTime = CurrentTime;
				bIsSettingsUpdated = true;
			}
		}
	}
	else
	{
		if(DeltaTime > IdleTXDelay)
		{
			Transmitter->SendRequest(Request);
			PreTime = CurrentTime;
			if(Request == ERequest::SaveRXSettings)
			{
				SetState(&StateSettings);
			}
			else
			{
				Request = ERequest::ChannelsValues;
			}
		}
	}
}




