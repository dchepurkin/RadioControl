#include "DState/DStateChannelsValues.h"
#include "DState/DStateTXSettings.h"
#include "DTransmitter.h"
#include "DColors.h"

DStateChannelsValues::DStateChannelsValues()
{
	Menu.Add("����� 1");
	Menu.Add("����� 2");
	Menu.Add("����� 3");
	Menu.Add("����� 4");
	Menu.Add("�����");
}

void DStateChannelsValues::OnBegin()
{
	PreTime = 0;
}

void DStateChannelsValues::Tick()
{
	DStateBase::Tick();

	if(CurrentTime - PreTime >= 100)
	{
		ShowChannelsValues();
		PreTime = CurrentTime;
	}
}

void DStateChannelsValues::DrawUI()
{
	Screen->DrawWindow("�������� �������");
	Menu.SetCursorPosition(4);
	Menu.Show(Screen);

	Screen->PrintString("%", {280, 40}, ETextAlign::Right);
	Screen->PrintString("%", {280, 65}, ETextAlign::Right);
	Screen->PrintString("%", {280, 90}, ETextAlign::Right);
	Screen->PrintString("%", {280, 115}, ETextAlign::Right);

}

void DStateChannelsValues::EnterButtonPressed()
{
	SetState(&StateTXSettings);
}

void DStateChannelsValues::ShowChannelsValues()
{
	if(!IsActive()) { return; }

	Screen->DrawFillRect({150, 40}, {106, 91}, BLACK);

	const auto ChannelsValues = Transmitter->GetChannelsValues();
	const auto ChannelsPercents = Transmitter->GetChannelsPercents();

	//Channel 1
	Screen->PrintString(std::to_string(ChannelsValues.Channel1).c_str(), {180, 40}, ETextAlign::Center);
	Screen->PrintString(std::to_string(ChannelsPercents.Channel1).c_str(), {256, 40}, ETextAlign::Right);

	//Channel 2
	Screen->PrintString(std::to_string(ChannelsValues.Channel2).c_str(), {180, 65}, ETextAlign::Center);
	Screen->PrintString(std::to_string(ChannelsPercents.Channel2).c_str(), {256, 65}, ETextAlign::Right);

	//Channel 3
	Screen->PrintString(std::to_string(ChannelsValues.Channel3).c_str(), {180, 90}, ETextAlign::Center);
	Screen->PrintString(std::to_string(ChannelsPercents.Channel3).c_str(), {256, 90}, ETextAlign::Right);

	//Channel 4
	Screen->PrintString(std::to_string(ChannelsValues.Channel4).c_str(), {180, 115}, ETextAlign::Center);
	Screen->PrintString(std::to_string(ChannelsPercents.Channel4).c_str(), {256, 115}, ETextAlign::Right);
}


