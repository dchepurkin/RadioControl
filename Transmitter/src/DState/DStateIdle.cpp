#include "DState/DStateIdle.h"
#include "DState/DStateSettings.h"
#include "DTransmitter.h"
#include "DColors.h"
#include "UI/DIdleStateScreen.h"
#include "pico/float.h"

void DStateIdle::OnBegin()
{
#ifdef DEBUG
	printf("State Idle enabled\n");
#endif

	Request = ERequest::BatteryStatus;
	ShowTXBattery();
}

void DStateIdle::DrawUI()
{
	Screen->DrawBitmap({0, 0}, {320, 170}, IdleStateScreen);
}

void DStateIdle::EnterButtonHoldDown()
{
	SetState(&StateSettings);
}

void DStateIdle::Tick()
{
	DStateBase::Tick();

	if(DeltaTime > IdleTXDelay)
	{
		Transmitter->SendRequest(Request);
		PreTime = CurrentTime;
	}

	const auto CurrentBatteryTime = to_ms_since_boot(get_absolute_time());
	if(CurrentBatteryTime - PreBatteryTime >= 15000)
	{
		Request = ERequest::BatteryStatus;
		PreBatteryTime = CurrentBatteryTime;
		ShowTXBattery();
	}

	if(Radio->isAckPayloadAvailable())
	{
		uint8_t Message[5];
		Radio->read(Message, 5);

		if(static_cast<ERequest>(Message[0]) == ERequest::BatteryStatus)
		{
			Request = ERequest::ChannelsValues;
			ShowRXBattery(Message[1]);
		}
	}
}

void DStateIdle::ShowTXBattery()
{
	ShowBattery(Transmitter->GetBatteryStatus(), 50, 90);
}

void DStateIdle::ShowRXBattery(uint8_t InBatteryPercent)
{
	ShowBattery(InBatteryPercent, 190, 230);
}

void DStateIdle::ShowBattery(uint8_t InBatteryPercent, const uint8_t InBatteryPositionX, const uint8_t InTextPositionX)
{
	if(InBatteryPercent > 100) { InBatteryPercent = 100; }

	constexpr float PercentPixels = .73;
	constexpr uint8_t Height = 40;
	constexpr uint8_t Width = 73;
	constexpr uint8_t CriticalBat = 30;

	const DVector2D BatteryPoint{InBatteryPositionX, 65};

	const uint8_t BatteryWidth = float2int(PercentPixels * InBatteryPercent);

	Screen->DrawFillRect(BatteryPoint, {BatteryWidth, Height}, InBatteryPercent > CriticalBat ? GREEN : RED);
	Screen->DrawFillRect(BatteryPoint + DVector2D{BatteryWidth, 0}, {Width - BatteryWidth, Height}, BLACK);
	Screen->DrawFillRect({InTextPositionX - 30, 124}, {60, 17}, BLACK);
	Screen->PrintString((std::to_string(InBatteryPercent) + " %").c_str(), {InTextPositionX, 124}, ETextAlign::Center);
}


