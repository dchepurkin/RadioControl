#include "DState/DStateSettings.h"
#include "DState/DStateIdle.h"
#include "DState/DStateRXSettings.h"
#include "DState/DStateTXSettings.h"

DStateSettings::DStateSettings()
{
	SettingsMenu.Add("��������� ������");
	SettingsMenu.Add("��������� ���������");
	SettingsMenu.Add("�����");
}

void DStateSettings::OnBegin()
{
#ifdef DEBUG
	printf("State Settings enabled\n");
#endif
}

void DStateSettings::DrawUI()
{
	Screen->DrawWindow("���������");
	SettingsMenu.Show(Screen);
}

void DStateSettings::UpButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::UP);
}

void DStateSettings::DownButtonPressed()
{
	SettingsMenu.MoveCursor(EDirection::DOWN);
}

void DStateSettings::EnterButtonPressed()
{
	const auto CursorPosition = SettingsMenu.GetCursorPosition();

	switch(CursorPosition)
	{
		case 0:
		{
			SetState(&StateTXSettings);
			break;
		}
		case 1:
		{
			SetState(&StateRXSettings);
			break;
		}
		case 2:
		{
			SettingsMenu.SetCursorPosition(0);
			SetState(&StateIdle);
			break;
		}
		default: break;
	}
}


