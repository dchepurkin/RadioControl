#include "UI/DMenu.h"
#include "UI/DCursor.h"
#include "DColors.h"

void DMenu::Show(DScreen* InScreen)
{
	Screen = InScreen;

	const uint8_t YPosition = 30;
	uint8_t Index = 0;
	DVector2D Point = TextOffset;

	for(const auto& Name : MenuList)
	{
		Point.Y = YPosition + Index * (YPosition - 5) + TextOffset.Y;
		Screen->PrintString(Name.c_str(), Point, ETextAlign::Left);

		++Index;
	}

	ShowCursor();
}

void DMenu::Add(std::string&& InName)
{
	MenuList.emplace_back(InName);
}

void DMenu::MoveCursor(EDirection InDirection)
{
	if(InDirection == EDirection::UP && GetCursorPosition() == 0) { return; }
	if(InDirection == EDirection::DOWN && GetCursorPosition() == MenuList.size() - 1) { return; }

	ClearCursor();
	SetCursorPosition(InDirection == EDirection::UP ? CursorPosition - 1 : CursorPosition + 1);

	ShowCursor();
}

void DMenu::ShowCursor()
{
	Screen->DrawBitmap(GetCursorCoords(), {20, 16}, Cursor);
}

void DMenu::ClearCursor()
{
	Screen->DrawFillRect(GetCursorCoords(), {20, 16}, BLACK);
}

DVector2D DMenu::GetCursorCoords()
{

	const uint8_t YPosition = 25;
	DVector2D Point;

	Point.X = 10;
	Point.Y = 30 + CursorPosition * YPosition + TextOffset.Y;

	return Point;
}

void DMenu::SetCursorPosition(const uint8_t InPosition)
{
	CursorPosition = InPosition;
}
