#include "DButton.h"

DButton::DButton(const uint InPin)
{
	Pin = InPin;

	gpio_init(Pin);
	gpio_set_dir(Pin, GPIO_IN);
}

void DButton::Tick()
{
	if(!bIsCheckState && gpio_get(Pin) != bIsPressed)
	{
		CheckState_Task();
	}
}

//����������� ��������� ��������� ������
DTask<StartRightNow> DButton::CheckState_Task()
{
	bIsCheckState = true;

	co_await DelayMillis(TriggerDelay); //�������� ��� ����� ��������
	if(gpio_get(Pin) != bIsPressed)
	{
		bIsPressed = !bIsPressed;

		if(bIsPressed)
		{
			OnPressed.Broadcast();

			//Start Hold timer, ���� ������ ��� ������� ��� �� ������� OnHoldDown ����� �� ��������, �� ������������
			if(OnHoldDown.IsBound())
			{
				HoldAlarmID = add_alarm_in_ms(HoldDelayTime, [](alarm_id_t id, void* user_data) -> int64_t
				{
					if(const auto Button = static_cast<DButton*>(user_data))
					{
						Button->StartHold();
					}
					return 0;
				}, this, false);
			}
		}
		else
		{
			OnReleased.Broadcast();

			//���� ������ ������� �� ��� ���������� ������� �������� ������
			if(HoldAlarmID)
			{
				cancel_alarm(HoldAlarmID);
				HoldAlarmID = 0;
			}
		}
	}

	bIsCheckState = false;
}

DTask<StartRightNow> DButton::HoldTick_Task()
{
	while(bIsPressed)
	{
		OnHoldDown.Broadcast();
		co_await DelayMillis(HoldTickDelta);
	}
}

void DButton::StartHold()
{
	HoldAlarmID = 0;

	if(HoldTickDelta)
	{
		HoldTick_Task();
	}
	else
	{
		OnHoldDown.Broadcast();
	}
}
