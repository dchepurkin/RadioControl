#pragma once

#include "pico/stdlib.h"
#include "DCoroutine.h"
#include "DDelegate.h"

DECLARE_DELEGATE(OnButtonEventSignature);

class DButton
{
public:
	OnButtonEventSignature OnPressed;
	OnButtonEventSignature OnReleased;
	OnButtonEventSignature OnHoldDown;

	DButton() = delete;

	explicit DButton(const uint InPin);

	uint GetPin() const { return Pin; }

	//Tick ������ ���������� � ����� �����
	void Tick();

	//������������� �������� � ������������� ��� ������� �������
	void SetHoldDelay(const uint InDelayMillis) { HoldDelayTime = InDelayMillis; }

	//������������� ������ ���� � ������������� ��� ������� �������, ���� 0 �� ������� ��������� ���� ���
	void SetHoldTickDelta(const uint InDeltaMillis) { HoldTickDelta = InDeltaMillis; }

	//���������� True - ���� ������� ������
	[[nodiscard]] bool IsPressed() const { return bIsPressed; }

private:
	uint Pin;

	uint8_t TriggerDelay = 50; //� �������������

	uint HoldDelayTime = 1000; //� �������������

	uint HoldTickDelta = 100; //� �������������

	bool bIsPressed = false;

	bool bIsCheckState = false;

	alarm_id_t HoldAlarmID = 0; //ID �������, ���� 0 �� ������ �� �������

	DTask<StartRightNow> CheckState_Task();

	DTask<StartRightNow> HoldTick_Task();

	void StartHold();
};