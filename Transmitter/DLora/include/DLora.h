#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "LoRa-RP2040.h"

class DLora
{
public:
	DLora();

	void Send(const uint8_t* InMessage, const uint8_t InSize);

	int Read();

	int ParsePacket();

	int IsAvailable() { return Lora.available(); }

	void SetSyncWord(int InWord) { Lora.setSyncWord(InWord); }

private:
	LoRaClass Lora;
};