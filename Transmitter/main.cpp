#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "DTransmitter.h"

#include "RF24.h"

int main()
{
#ifdef DEBUG
	stdio_init_all();
#endif

	DTransmitter Transmitter;
	Transmitter.Begin();

	return 0;
}
