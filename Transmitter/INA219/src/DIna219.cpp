#include "DIna219.h"

DIna219::DIna219(uint InSDAPin, uint InSCLPin)
{
	Ina.I2C_START(InSDAPin, InSCLPin, 400);
	Ina.calibrate(0.1, 3.2);
}

uint8_t DIna219::GetBatteryLevel()
{
	const float Voltage = Ina.read_voltage();
	const float ShuntVoltage = Ina.read_shunt_voltage();

	const auto V = Voltage + (ShuntVoltage / 1000);

	uint8_t Index = 0;
	for(; Index < 10; ++Index)
	{
		if(V > BatLevels[Index]) { break; }
	}

	uint8_t BatPercent = 100;
	if(Index > 0)
	{
		uint8_t SubPercent = (V - BatLevels[Index]) * 10 / (BatLevels[Index - 1] - BatLevels[Index]);
		BatPercent = BatPercent - Index * 10 + SubPercent;
	}

	return BatPercent;
}
