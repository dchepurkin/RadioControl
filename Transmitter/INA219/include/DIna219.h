#pragma once

#include "pico/stdlib.h"
#include "INA219.h"

class DIna219
{
public:
	DIna219(uint InSDAPin, uint InSCLPin);

	uint8_t GetBatteryLevel();

private:
	const float BatLevels[10] = {4.08, 4.0, 3.82, 3.76, 3.65, 3.6, 3.5, 3.4, 3.3, 3.0};

	INA219 Ina{i2c1, 0x40};
};