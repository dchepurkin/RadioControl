#pragma once

#include "pico/stdlib.h"

const uint8_t ConnectAnswer = 155;

const uint8_t IdleTXDelay = 25;

#define CALIBRATION_SETTINGS_OFFSET (PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE)
#define INVERT_SETTINGS_OFFSET (PICO_FLASH_SIZE_BYTES - FLASH_SECTOR_SIZE * 2)

enum class ERequest : uint8_t
{
	ConnectionStatus,
	BatteryStatus,
	ChannelsValues,
	GetRXSettings,
	RXSettings,
	SaveRXSettings,
};

struct ChannelsPercents
{
public:
	int8_t Channel1 = 0;

	int8_t Channel2 = 0;

	int8_t Channel3 = 0;

	int8_t Channel4 = 0;
};

struct ChannelsValues
{
public:
	uint16_t Channel1 = 0;

	uint16_t Channel2 = 0;

	uint16_t Channel3 = 0;

	uint16_t Channel4 = 0;
};

struct RXSettings
{
	int8_t MaxEnginePower = 100;

	int8_t Channel2ServoOffset = 0;

	int8_t Channel3ServoOffset = 0;

	int8_t Channel4ServoOffset = 0;
};

struct DMinMaxChannel
{
	uint16_t Min = 0;

	uint16_t Max = 0;
};

struct DChannelsInvertInfo
{
	bool bIsLeftVInverted : 1 = false;

	bool bIsLeftHInverted : 1 = false;

	bool bIsRightVInverted : 1 = false;

	bool bIsRightHInverted : 1 = false;
};

struct DChannelsInfo
{
	DMinMaxChannel LeftV;

	DMinMaxChannel LeftH;

	DMinMaxChannel RightV;

	DMinMaxChannel RightH;
};