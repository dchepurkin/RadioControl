#pragma once

#include "pico/stdlib.h"
#include "DScreen.h"
#include <list>
#include <string>

enum class EDirection : uint8_t
{
	UP,
	DOWN
};

class DMenu
{
public:
	void Show(DScreen* InScreen);

	void Add(std::string&& InName);

	uint8_t GetCursorPosition() const { return CursorPosition; }

	void MoveCursor(EDirection InDirection);

	void SetCursorPosition(const uint8_t InPosition);

private:
	std::list<std::string> MenuList;

	DVector2D TextOffset{40, 10};

	uint8_t CursorPosition = 0;

	void ShowCursor();

	void ClearCursor();

	DVector2D GetCursorCoords();

	DScreen* Screen = nullptr;
};