#pragma once

#include "DStateBase.h"
#include "UI/DMenu.h"
#include "DRadioTypes.h"

inline class DStateRXSettings : public DStateBase
{
public:
	DStateRXSettings();

	void OnBegin() override;

	void Tick() override;

	void DrawUI() override;

	void UpButtonPressed() override;

	void DownButtonPressed() override;

	void LeftButtonPressed() override;

	void RightButtonPressed() override;

	void EnterButtonPressed() override;

private:
	DMenu Menu;

	bool bIsSettingsUpdated = false;

	ERequest Request = ERequest::ChannelsValues;

	void ShowRXSettings();
} StateRXSettings;