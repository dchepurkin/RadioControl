#pragma once

#include "DStateBase.h"
#include "UI/DMenu.h"

inline class DStateChannelsValues : public DStateBase
{
public:
	DStateChannelsValues();

	void OnBegin() override;

	void Tick() override;

	void DrawUI() override;

	void EnterButtonPressed() override;

private:
	DMenu Menu;

	void ShowChannelsValues();
} StateChannelsValues;