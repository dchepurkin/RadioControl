#pragma once

#include "DStateBase.h"
#include "UI/DMenu.h"

inline class DStateTXSettings : public DStateBase
{
public:
	DStateTXSettings();

	void OnBegin() override;

	void DrawUI() override;

	void UpButtonPressed() override;

	void DownButtonPressed() override;

	void EnterButtonPressed() override;

	void SetConnectingOnQuit() { bIsConnectingOnQuit = true; }

private:
	DMenu SettingsMenu;

	bool bIsConnectingOnQuit = false;
} StateTXSettings;