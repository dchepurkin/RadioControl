#pragma once

#include "DStateBase.h"
#include "UI/DMenu.h"

inline class DStateSettings : public DStateBase
{
public:
	DStateSettings();

	void OnBegin() override;

	void DrawUI() override;

	void UpButtonPressed() override;

	void DownButtonPressed() override;

	void EnterButtonPressed() override;

private:
	DMenu SettingsMenu;
} StateSettings;