#pragma once

#include "pico/stdlib.h"
#include "DScreen.h"
#include "RF24.h"
#include "DRadioTypes.h"

class DStateBase
{
public:
	virtual ~DStateBase() = default;

	void Begin(class DTransmitter* InTransmitter);

	virtual void Tick();

	virtual void EnterButtonPressed() {}

	virtual void EnterButtonHoldDown() {}

	virtual void UpButtonPressed() {}

	virtual void DownButtonPressed() {}

	virtual void LeftButtonPressed() {}

	virtual void RightButtonPressed() {}

	void SetState(DStateBase* InState);

	bool IsActive() const { return bIsActive; }

protected:
	virtual void OnBegin() = 0;

	virtual void DrawUI() = 0;

	class DTransmitter* Transmitter = nullptr;

	RF24* Radio = nullptr;

	DScreen* Screen = nullptr;

	bool bIsActive = false;

	uint32_t PreTime = 0;

	uint32_t CurrentTime = 0;

	uint32_t DeltaTime = 0;
};