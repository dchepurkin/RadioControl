#pragma once

#include "DStateBase.h"
#include "DRadioTypes.h"
#include "UI/DMenu.h"

inline class DStateInvertSettings : public DStateBase
{
public:
	DStateInvertSettings();

	void OnBegin() override;

	void Tick() override;

	void DrawUI() override;

	void UpButtonPressed() override;

	void DownButtonPressed() override;

	void EnterButtonPressed() override;

private:
	DMenu SettingsMenu;

	DChannelsInvertInfo InvertInfo;

	void ShowInvertSettings();

} StateInvertSettings;