#pragma once

#include "DStateBase.h"

inline class DStateConnecting : public DStateBase
{
public:
	void OnBegin() override;

	void Tick() override;

	void DrawUI() override;

	void EnterButtonHoldDown() override;

private:
	void DrawConnectionScreen();

	void DrawConnectionSuccess();

	uint8_t ScreenIndex = 0;

	uint32_t PreScreenTime = 0;

	bool IsConnected = false;
} StateConnecting;