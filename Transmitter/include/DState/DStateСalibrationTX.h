#pragma once

#include "DStateBase.h"
#include "DRadioTypes.h"

inline class DStateCalibrationRX : public DStateBase
{
public:
	void OnBegin() override;

	void DrawUI() override;

	void EnterButtonPressed() override;

private:
	enum ECalibrationState : uint8_t
	{
		LeftVerticalMin = 0,
		LeftVerticalMax,
		LeftHorizontalMin,
		LeftHorizontalMax,
		RightVerticalMin,
		RightVerticalMax,
		RightHorizontalMin,
		RightHorizontalMax,
		Done,
	} State;

	DChannelsInfo Settings;

	void ShowHelpText();
} StateCalibrationTX;