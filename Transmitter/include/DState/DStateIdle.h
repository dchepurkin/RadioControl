#pragma once

#include "DStateBase.h"

inline class DStateIdle : public DStateBase
{
public:
	void OnBegin() override;

	void Tick() override;

	void DrawUI() override;

	void EnterButtonHoldDown() override;

private:
	ERequest Request = ERequest::BatteryStatus;

	uint32_t PreBatteryTime = 0;

	void ShowTXBattery();

	void ShowRXBattery(uint8_t InBatteryPercent);

	void ShowBattery(uint8_t InBatteryPercent, const uint8_t InBatteryPositionX, const uint8_t InTextPositionX);

} StateIdle;