#pragma once

#include "pico/stdlib.h"
#include "DRadioTypes.h"
#include <map>

class DJoystick
{
public:
	explicit DJoystick(const uint InHPin, const uint InVPin);

	int8_t GetHValue();

	int8_t GetVValue();

	bool IsHInverted() const { return bIsHInverted; }

	bool IsVInverted() const { return bIsVInverted; }

	uint16_t GetHADCValue();

	uint16_t GetVADCValue();

	void SetInverted(bool Horizontal, bool Vertical);

	void SetMinMax(const DMinMaxChannel& InVMinMaxInfo, const DMinMaxChannel& InHMinMaxInfo);

private:
	uint HPin;

	uint VPin;

	bool bIsHInverted = false;

	bool bIsVInverted = false;

	std::map<uint8_t, uint8_t> ADCChannels = {{  26, 0}
											  , {27, 1}
											  , {28, 2}
											  , {29, 3}};

	DMinMaxChannel HMinMax;

	DMinMaxChannel VMinMax;

	int8_t GetValue(uint InPin, const DMinMaxChannel& InMinMaxInfo);

	uint16_t GetADCValue(uint InPin);
};