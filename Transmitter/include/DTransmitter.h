#pragma once

#ifdef DEBUG
	#include <cstdio>
#endif

#include "pico/stdlib.h"
#include "DScreen.h"
#include "DCoroutine.h"
#include "DButton.h"
#include "DState/DStateBase.h"
#include "DRadioTypes.h"
#include "DJoystick.h"
#include "DIna219.h"

#include "RF24.h"

class DTransmitter
{
public:
	void Begin();

	void SetState(DStateBase* InState);

	DScreen* GetScreen() { return &Screen; }

	RF24* GetRadio() { return &Radio; }

	uint8_t GetBatteryStatus();

	ChannelsValues GetChannelsValues();

	ChannelsPercents GetChannelsPercents();

	void SendRequest(ERequest InRequest);

	void SetRXSettings(const RXSettings& InSettings);

	const RXSettings& GetRXSettings() const { return RxSettings; }

	void AddRXSettingsValues(const RXSettings& InSettings);

	void SaveCalibrationInfo(const DChannelsInfo& InInfo);

	void SaveInvertInfo(const DChannelsInvertInfo& InInfo);

	DJoystick& GetLeftJoystick() { return LeftJoystick; }

	DJoystick& GetRightJoystick() { return RightJoystick; }

	void GetInvertInfo(DChannelsInvertInfo& OutInfo);

	void UpdateJoysticksInvertion(const DChannelsInvertInfo& InSettings);

private:
	RF24 Radio{5, 6};

	DIna219 Ina{14, 15};

	DScreen Screen;

	class DStateBase* State = nullptr;

	RXSettings RxSettings;

	DButton UpButton{16};

	DButton LeftButton{17};

	DButton DownButton{18};

	DButton RightButton{19};

	DButton EnterButton{20};

	DJoystick LeftJoystick{27, 26};

	DJoystick RightJoystick{29, 28};

	void OnUpButtonPressed();

	void OnLeftButtonPressed();

	void OnDownButtonPressed();

	void OnRightButtonPressed();

	void OnEnterButtonPressed();

	void OnEnterButtonHold();

	void ReadSettingsFromFlash();

	void UpdateJoysticksCalibration(const DChannelsInfo& InSettings);
};