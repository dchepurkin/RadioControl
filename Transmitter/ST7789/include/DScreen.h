#pragma once

#include <pico/stdlib.h>
#include <hardware/pio.h>
#include "DScreenTypes.h"

class DScreen
{
public:
	DScreen()
			: DScreen(DScreenInfo{}) {};

	explicit DScreen(const DScreenInfo& InScreenInfo);

	[[nodiscard]] const DVector2D& GetScreenSize() const { return ScreenInfo.Size; }

	[[nodiscard]] int GetWidth() const { return GetScreenSize().X; }

	[[nodiscard]] int GetHeight() const { return GetScreenSize().Y; }

	void SetLedEnabled(const bool IsEnabled) const;

	void SetRotation(ERotation InRotation);

	void FillScreen(const uint16_t InColor) const;
	void DrawBitmap(const DVector2D& InPoint, const DVector2D& InSize, const uint16_t* InBitmap) const;
	void DrawPixel(const DVector2D& InPoint, const uint16_t InColor) const;
	void DrawHLine(const DVector2D& InStartPoint, const int InLength, const uint16_t InColor) const;
	void DrawVLine(const DVector2D& InStartPoint, const int InLength, const uint16_t InColor) const;
	void DrawFillRect(const DVector2D& InPoint, const DVector2D& InSize, const uint16_t InColor) const;
	void DrawRect(const DVector2D& InPoint, const DVector2D& InSize, const int InBorderSize, const uint16_t InBorderColor, const bool Fill = false, const uint16_t InFillColor = 0xFFFF) const;
	void DrawWindow(const char* InWindowName);

	void PrintString(const char* InString, const DVector2D& InPoint);
	void PrintString(const char* InString, DVector2D InPoint, ETextAlign InAlign);
	uint8_t GetTextLength(const char* InString);

private:
	PIO Pio = pio0;

	const uint SMNum = 0;

	DScreenInfo ScreenInfo;

	void InitScreen() const;

	void WriteSeq(const uint8_t* InSeq) const;

	void WriteCMD(const uint8_t* InCmd, size_t InCount) const;

	void SetDCCS(bool DC, bool CS) const;

	void StartPixels(const DVector2D& InPoint1, const DVector2D& InPoint2) const;

	void PutPixel(const uint16_t InColor) const;

	void SetOffSet(const DVector2D& InOffset);

	const uint16_t* GetCharStartPointer(const uint8_t InChar);
};