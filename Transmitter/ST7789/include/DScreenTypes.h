#pragma once

#include <pico/stdlib.h>
#include "DVector2D.h"

enum class ETextAlign : uint8_t
{
	Left,
	Right,
	Center,
};

enum class ERotation
{
	Degree0 = 0xD0,    //0 degrees
	Degree90 = 0xA0,    //90 degrees
	Degree180 = 0x00,    //180 degrees
	Degree270 = 0x60    //270 degrees
};

struct DScreenInfo
{
	uint8_t PinSDA = 9;

	uint8_t PinSCK = 8;

	uint8_t PinCS = 12;

	uint8_t PinA0 = 11; //dc

	uint8_t PinRST = 10;

	uint8_t PinLED = 13;

	ERotation Rotation = ERotation::Degree0;

	float SerialClkDiv = 1.0;

	DVector2D Size{170, 320};

	DVector2D Offset{0, 0};
};