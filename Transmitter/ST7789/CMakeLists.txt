cmake_minimum_required(VERSION 3.13)

project(ST7789)

set(HEADER_FILES
        include/DScreen.h
        include/DScreenTypes.h
        include/DColors.h
        include/FontBlack.h
        include/DVector2D.h)

set(SOURCE_FILES
        src/DScreen.cpp
        src/DVector2D.cpp)

add_library(ST7789 ${SOURCE_FILES} ${HEADER_FILES})

pico_generate_pio_header(ST7789 ${CMAKE_CURRENT_LIST_DIR}/ST7789_Driver.pio)

target_link_libraries(ST7789 pico_stdlib hardware_pio)

target_include_directories(ST7789 PUBLIC include/)
target_include_directories(ST7789 PRIVATE src/)

