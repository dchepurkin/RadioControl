#pragma once

#include <list>
#include <memory>
#include <algorithm>
#include "DFunction.h"

#define DECLARE_DELEGATE(DelegateName) using DelegateName = DDelegate;

#define DECLARE_DELEGATE_OneParam(DelegateName, ParamType) using DelegateName = DDelegateOneParam<ParamType>;

#define DECLARE_DELEGATE_TwoParams(DelegateName, Param1Type, Param2Type) using DelegateName = DDelegateTwoParams<Param1Type, Param2Type>;

class DDelegate
{
public:
	DDelegate() = default;

	~DDelegate() = default;

	DDelegate(const DDelegate&) = delete;

	DDelegate& operator=(const DDelegate&) = delete;

	template<typename UserClass>
	void Bind(UserClass* InObject, Method<UserClass> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver == Observers.end())
		{
			Observers.emplace_front(std::make_unique<DObjectMethod<UserClass>>(InObject, InCallback));
		}
	}

	void Bind(Func InCallback);

	template<typename UserClass>
	void Unbind(UserClass* InObject, Method<UserClass> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver != Observers.end())
		{
			Observers.remove(*FoundObserver);
		}
	}

	void Unbind(Func InCallback);

	void Broadcast()
	{
		for(const auto& Observer : Observers)
		{
			Observer->Execute();
		}
	}

	[[nodiscard]] bool IsBound() const { return !Observers.empty(); }

	void Clear() { Observers.clear(); }

private:
	std::list<std::unique_ptr<DCallback>> Observers;

	template<typename UserClass>
	auto FindObserver(UserClass* InObject, Method<UserClass> InCallback)
	{
		return std::find_if(Observers.begin(), Observers.end(), [InObject, InCallback](std::unique_ptr<DCallback>& InObserver)
		{
			if(const auto TObserver = static_cast<DObjectMethod<UserClass>*>(InObserver.get()))
			{
				return *TObserver == DObjectMethod{InObject, InCallback};
			}
			return false;
		});
	}

	auto FindObserver(Func InFunc);
};

////////////////////////////////////////////////////////////////////////////////////

template<typename ParamType>
class DDelegateOneParam
{
public:
	DDelegateOneParam() = default;

	~DDelegateOneParam() = default;

	DDelegateOneParam(const DDelegateOneParam&) = delete;

	DDelegateOneParam& operator=(const DDelegateOneParam&) = delete;

	template<typename UserClass>
	void Bind(UserClass* InObject, MethodOneParam<UserClass, ParamType> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver == Observers.end())
		{
			Observers.emplace_front(std::make_unique<DObjectMethodOneParam<UserClass, ParamType>>(InObject, InCallback));
		}
	}

	template<typename UserClass>
	void Unbind(UserClass* InObject, MethodOneParam<UserClass, ParamType> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver != Observers.end())
		{
			Observers.remove(*FoundObserver);
		}
	}

	void Broadcast(ParamType InValue)
	{
		for(const auto& Observer : Observers)
		{
			Observer->Execute(InValue);
		}
	}

	void Clear() { Observers.clear(); }

	[[nodiscard]] bool IsBound() const { return !Observers.empty(); }

private:
	std::list<std::unique_ptr<DCallbackOneParam<ParamType>>> Observers;

	template<typename UserClass>
	auto FindObserver(UserClass* InObject, MethodOneParam<UserClass, ParamType> InCallback)
	{
		return std::find_if(Observers.begin(), Observers.end(), [InObject, InCallback](std::unique_ptr<DCallbackOneParam<ParamType>>& InObserver)
		{
			if(const auto TObserver = static_cast<DObjectMethodOneParam<UserClass, ParamType>*>(InObserver.get()))
			{
				return *TObserver == DObjectMethodOneParam<UserClass, ParamType>{InObject, InCallback};
			}
			return false;
		});
	}
};

////////////////////////////////////////////////////////////////////////////////////

template<typename Param1Type, typename Param2Type>
class DDelegateTwoParams
{
public:
	DDelegateTwoParams() = default;

	~DDelegateTwoParams() = default;

	DDelegateTwoParams(const DDelegateTwoParams&) = delete;

	DDelegateTwoParams& operator=(const DDelegateTwoParams&) = delete;

	template<typename UserClass>
	void Bind(UserClass* InObject, MethodTwoParams<UserClass, Param1Type, Param2Type> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver == Observers.end())
		{
			Observers.emplace_front(std::make_unique<DObjectMethodTwoParams<UserClass, Param1Type, Param2Type>>(InObject, InCallback));
		}
	}

	template<typename UserClass>
	void Unbind(UserClass* InObject, MethodTwoParams<UserClass, Param1Type, Param2Type> InCallback)
	{
		const auto FoundObserver = FindObserver(InObject, InCallback);

		if(FoundObserver != Observers.end())
		{
			Observers.remove(*FoundObserver);
		}
	}

	void Broadcast(Param1Type InValue1, Param2Type InValue2)
	{
		for(const auto& Observer : Observers)
		{
			Observer->Execute(InValue1, InValue2);
		}
	}

	void Clear() { Observers.clear(); }

	[[nodiscard]] bool IsBound() const { return !Observers.empty(); }

private:
	std::list<std::unique_ptr<DCallbackTwoParams<Param1Type, Param2Type>>> Observers;

	template<typename UserClass>
	auto FindObserver(UserClass* InObject, MethodTwoParams<UserClass, Param1Type, Param2Type> InCallback)
	{
		return std::find_if(Observers.begin(), Observers.end(), [InObject, InCallback](std::unique_ptr<DCallbackTwoParams<Param1Type, Param2Type>>& InObserver)
		{
			if(const auto TObserver = static_cast<DObjectMethodTwoParams<UserClass, Param1Type, Param2Type>*>(InObserver.get()))
			{
				return *TObserver == DObjectMethodTwoParams<UserClass, Param1Type, Param2Type>{InObject, InCallback};
			}
			return false;
		});
	}
};
