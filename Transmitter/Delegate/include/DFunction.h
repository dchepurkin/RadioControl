#pragma once

using Func = void (*)();

template<class ParamType>
using FuncOneParam = void (*)(ParamType);

template<class UserClass>
using Method = void (UserClass::*)();

template<class UserClass, class ParamType>
using MethodOneParam = void (UserClass::*)(ParamType);

template<class UserClass, class Param1Type, class Param2Type>
using MethodTwoParams = void (UserClass::*)(Param1Type, Param2Type);

class DCallback
{
public:
	virtual ~DCallback() = default;

	virtual void Execute() = 0;

	virtual bool operator==(void* InObject) = 0;
};

class DFunction : public DCallback
{
public:
	DFunction(Func InCallback)
			: Callback(InCallback) {}

	void Execute() override
	{
		if(Callback) { Callback(); }
	}

	bool operator==(void* InObject) override
	{
		return Callback == static_cast<DFunction*>(InObject)->Callback;
	}

	bool operator==(const DFunction& Other) const
	{
		return Callback == Other.Callback;
	}

private:
	Func Callback = nullptr;
};

template<typename UserClass>
class DObjectMethod : public DCallback
{
public:
	DObjectMethod(UserClass* InObject, Method<UserClass> InMethod)
			: Object(InObject),
			  Callback(InMethod) {}

	void Execute() override
	{
		if(!Object)
		{
			return;
		}

		(Object->*Callback)();
	}

	bool operator==(void* InObject) override
	{
		return InObject == Object;
	}

	bool operator==(const DObjectMethod& Other) const
	{
		return Object == Other.Object && Callback == Other.Callback;
	}

private:
	UserClass* Object = nullptr;

	Method<UserClass> Callback = nullptr;
};

/////////////////////////////////////////////////////////////////////////////////////////

template<typename ParamType>
class DCallbackOneParam
{
public:
	virtual ~DCallbackOneParam() = default;

	virtual void Execute(ParamType ParamValue) = 0;

	virtual bool operator==(void* InObject) = 0;
};

template<typename UserClass, typename ParamType>
class DObjectMethodOneParam : public DCallbackOneParam<ParamType>
{
public:
	DObjectMethodOneParam(UserClass* InObject, MethodOneParam<UserClass, ParamType> InMethod)
			: Object(InObject),
			  Callback(InMethod) {}

	void Execute(ParamType ParamValue) override
	{
		(Object->*Callback)(ParamValue);
	}

	bool operator==(void* InObject) override
	{
		return InObject == Object;
	}

	bool operator==(const DObjectMethodOneParam& Other) const
	{
		return Object == Other.Object && Callback == Other.Callback;
	}

private:
	UserClass* Object = nullptr;

	MethodOneParam<UserClass, ParamType> Callback = nullptr;
};

/////////////////////////////////////////////////////////////////////////////////////////

template<typename Param1Type, typename Param2Type>
class DCallbackTwoParams
{
public:
	virtual ~DCallbackTwoParams() = default;

	virtual void Execute(Param1Type Param1Value, Param2Type Param2Value) = 0;

	virtual bool operator==(void* InObject) = 0;
};

template<typename UserClass, typename Param1Type, typename Param2Type>
class DObjectMethodTwoParams : public DCallbackTwoParams<Param1Type, Param2Type>
{
public:
	DObjectMethodTwoParams(UserClass* InObject, MethodTwoParams<UserClass, Param1Type, Param2Type> InMethod)
			: Object(InObject),
			  Callback(InMethod) {}

	void Execute(Param1Type Param1Value, Param2Type Param2Value) override
	{
		(Object->*Callback)(Param1Value, Param2Value);
	}

	bool operator==(void* InObject) override
	{
		return InObject == Object;
	}

	bool operator==(const DObjectMethodTwoParams& Other) const
	{
		return Object == Other.Object && Callback == Other.Callback;
	}

private:
	UserClass* Object = nullptr;

	MethodTwoParams<UserClass, Param1Type, Param2Type> Callback = nullptr;
};