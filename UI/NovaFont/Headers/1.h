
/*******************************************************************************
* generated by lcd-image-converter rev.030b30d from 2019-03-17 01:38:34 +0500
* image
* filename: unsaved
* name: 1
*
* preset name: Цветное R5G6B5
* data block size: 16 bit(s), uint16_t
* RLE compression enabled: no
* conversion type: Color, not_used not_used
* split to rows: yes
* bits per pixel: 16
*
* preprocess:
*  main scan direction: top_to_bottom
*  line scan direction: forward
*  inverse: no
*******************************************************************************/

/*
 typedef struct {
     const uint16_t *data;
     uint16_t width;
     uint16_t height;
     uint8_t dataSize;
     } tImage;
*/
#include <stdint.h>



static const uint16_t image_data_1[272] = {
    // ∙∙▒▓▓▒░∙∙∙∙∙∙∙∙∙∙
    // ∙∙░▓▒▒░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙∙▒∙∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙∙▒∙∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ∙∙∙∙░▓░∙∙∙∙∙∙∙∙∙∙
    // ░▓▓▓▒▒▒▓▓▓░∙∙∙∙∙∙
    // ░▓▓▓▒∙▒▓▓▓░∙∙∙∙∙∙
    0x0000, 0x0020, 0x0534, 0x07ff, 0x07ff, 0x0679, 0x0269, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x04b2, 0x077d, 0x0638, 0x06db, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0124, 0x0618, 0x00e3, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0124, 0x0618, 0x00e3, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0000, 0x0000, 0x0000, 0x0000, 0x0430, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x038e, 0x077d, 0x077d, 0x077d, 0x0638, 0x05b6, 0x0679, 0x077d, 0x077d, 0x077d, 0x030c, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 
    0x0471, 0x07ff, 0x07ff, 0x07ff, 0x0618, 0x0104, 0x06ba, 0x07ff, 0x07ff, 0x07ff, 0x038e, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000, 0x0000
};
const tImage 1 = { image_data_1, 17, 16,
    16 };

